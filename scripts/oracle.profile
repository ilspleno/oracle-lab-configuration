#!/bin/bash

# Initialize shell environment

alias oraenv='. oraenv && . oraenv_local'

# Get a database to source
SID=`grep -v ASM /etc/oratab | grep -v MGMTDB | tail -1 | cut -f1 -d:`

if [ "${SID}x" == "x" ]; then
	# No available database, don't do anything
	echo 
	echo "No database found in /etc/oratab, skipping environment setup"
	echo
else
	echo 
	echo "Setting up oracle environment using $SID"
	export ORAENV_ASK=NO
	export ORACLE_SID=$SID
	oraenv	

fi
